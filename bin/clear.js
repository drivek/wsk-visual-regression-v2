const path = require('path');
const fs = require('fs');
const rimraf = require('rimraf');

const baselineDirectory = path.resolve(__dirname, '..', '..', 'baseline');
const outputDirectory = path.resolve(__dirname, '..', '..', 'output');

rimraf.sync(baselineDirectory);
rimraf.sync(outputDirectory);

if (!fs.existsSync(baselineDirectory)) {
  fs.mkdirSync(baselineDirectory);
}

if (!fs.existsSync(outputDirectory)) {
  fs.mkdirSync(outputDirectory);
}
