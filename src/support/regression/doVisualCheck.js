import _ from 'lodash';

/**
 *
 * @param {String} article
 * @param {String} name
 * @param {String} type
 * @param {String} selector
 */
export default (article, name, type, selector) => {
  if (!_.isEmpty(selector)) {
    const nrOfElements = $$(selector);
    const element = nrOfElements[0];

    return browser.checkElement(element, name);
  }

  if (type === 'screen') {
    return browser.checkScreen(name);
  }

  return browser.checkFullPageScreen(name);
};
