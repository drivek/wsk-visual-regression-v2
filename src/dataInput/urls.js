const autostile = {
  beta: 'autostile.websparksite-it.test-revo.feature.dealerk.com',
  prod: 'autostilespa.com',
};

const getEnv = () => process.env.ENV;

const switchEnv = () => {
  const option = getEnv();
  const env = (option === 'PROD') ? autostile.prod : autostile.beta;
  return env;
};

const urls = {
  home: (dominio) => `https://www.${dominio}`,
  autostileVdp: (dominio) => `https://www.${dominio}/auto/nuove-pronta-consegna/reggio-emilia/alfa-romeo/giulietta/benzina/1-4-turbo-120-cv-super/3297180/`,
  configAuto: (dominio) => `https://www.${dominio}/auto/nuove/fiat/`,
  infoService: (dominio) => `https://www.${dominio}/officina-reggio-emilia/`,
  sedi: (dominio) => `https://www.${dominio}/sedi/`,
  model: (dominio) => `https://www.${dominio}/auto/nuove/abarth/500/benzina/1-4-t-jet-595-turismo-165cv/2019/1000260332/`,
};

const errebiAuto = {
  home: 'https://energycar.errebiauto.it/',
  configAuto: 'https://energycar.errebiauto.it/auto/nuove/renault/',
  model: 'https://energycar.errebiauto.it/auto/nuove/renault/espace/crossover-5-porte/2015/',
  sedi: 'https://energycar.errebiauto.it/sedi/',
  infoServiceGomme: 'https://energycar.errebiauto.it/convergenza-gomme/',
  vdpStock: 'https://energycar.errebiauto.it/auto/usate/alessandria/renault/trafic/diesel/trafic-t29-2-0-dci-115-pl-ta-furgone-dpf/3829461/',
};

const gtomotors = {
  home: 'https://www.gtomotors.it/',
  configAuto: 'https://www.gtomotors.it/auto/nuove/ferrari/',
  model: 'https://www.gtomotors.it/auto/nuove/ferrari/812-gts/cabriolet-2-porte/2020/',
  sedi: 'https://www.gtomotors.it/sedi/',
  infoService: 'https://www.gtomotors.it/servizi-finanziari-ferrari/',
  vdpStock: 'https://www.gtomotors.it/auto/usate/varese/ferrari/488-gtb/benzina/tailor-made-unica/3821578/',
};

const autoLocatelli = {
  home: 'https://www.lineautolocatelli.com/',
  infoService: 'https://www.lineautolocatelli.com/finanziamento-personalizzato-lineauto/',
  vdpStock: 'https://www.lineautolocatelli.com/auto/usate/bergamo/fiat/croma/diesel/croma-1-9-multijet-emotion/3779038/',
  sedi: 'https://www.lineautolocatelli.com/sedi/',
};

const peila = {
  home: 'https://www.peila.it/',
  configAuto: 'https://www.peila.it/auto/nuove/suzuki/',
  model: 'https://www.peila.it/auto/nuove/suzuki/baleno/berlina-5-porte/2016/',
  sedi: 'https://www.peila.it/sedi/',
  infoService: 'https://www.peila.it/servizi-finanziari-aosta-torino/',
  vdpStock: 'https://www.peila.it/auto/usate/torino/citroen/c-crosser/diesel/2-2-hdi-160cv-fap/1334451/',
};

const gruppoMorini = {
  infoBanner: '//*[@id="popup_box_close_2948"]',
  home: 'https://www.gruppomorini.it',
  configAuto: 'https://www.gruppomorini.it/auto/nuove/toyota/',
  model: 'https://www.gruppomorini.it/auto/nuove/toyota/aygo/citycar-5-porte/2018/',
  sedi: 'https://www.gruppomorini.it/sedi/',
  infoService: 'https://www.gruppomorini.it/tagliando-auto-emilia-romagna/',
  vdpStock: 'https://www.gruppomorini.it/auto/usate/bologna/autobianchi/bianchina/benzina/bianchina-110-d/3056109/',

};

const lodAuto = {
  notificationPushDismiss: '/html/body/div[3]/div/div[3]/div[2]/button[1]',
  home: 'https://www.lodauto.it',
  configAuto: 'https://www.lodauto.it/auto/nuove/mercedes-benz/',
  model: 'https://www.lodauto.it/auto/nuove/mercedes-benz/amg-gt/coupe-3-porte/2019/',
  sedi: 'https://www.lodauto.it/sedi/',
  infoService: 'https://www.lodauto.it/assistenza-mercedes-benz-bergamo/',
  vdpStock: 'https://www.lodauto.it/auto/usate/bergamo/mercedes-benz/gle/diesel/gle-250-d-4matic-sport/3870656/',
};

const autoVarese = {
  home: 'https://www.autoeautovarese.it',
  infoService: 'https://www.autoeautovarese.it/estensione-garanzia-mapfre/',
  vdpStock: 'https://www.autoeautovarese.it/auto/usate/varese/seat/leon/diesel/1-6-tdi-110-cv-dsg-5p-start-stop-style/3911618/',
  sedi: 'https://www.autoeautovarese.it/sedi/',
};

const marAutogroup = {
  home: 'https://www.mar-autogroup.it',
  configAuto: 'https://www.mar-autogroup.it/auto/nuove/ford/',
  model: 'https://www.mar-autogroup.it/auto/nuove/ford/ecosport/suv-5-porte/2017/',
  sedi: 'https://www.mar-autogroup.it/sedi/',
  infoService: 'https://www.mar-autogroup.it/officina/',
  vdpStock: 'https://www.mar-autogroup.it/auto/usate/treviso/fiat/scudo/diesel/2-0-mjt-pc-tn-furgone-12q-comfort/3662788/',
};

const fiatPavan = {
  home: 'https://www.fiatpavan.it',
  configAuto: 'https://www.fiatpavan.it/auto/nuove/fiat/',
  model: 'https://www.fiatpavan.it/auto/nuove/fiat/500/berlina-2-vol-3-porte/2015/',
  sedi: 'https://www.fiatpavan.it/sedi/',
  infoService: 'https://www.fiatpavan.it/assistenza-auto-piove-di-sacco/',
  vdpStock: 'https://www.fiatpavan.it/auto/usate/padova/lancia/ypsilon/benzina/1-2-platino-80cv/3838827/',
};

export {
  urls, autoLocatelli, errebiAuto, gtomotors, peila,
  gruppoMorini, lodAuto, autoVarese, marAutogroup, fiatPavan, switchEnv,
};
