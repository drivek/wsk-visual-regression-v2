const baseCaps = {
  elektraUrls: [
    'https://demo-it.wsr74.dealerk.com/elektra/badge/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/button/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/toggle/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/image/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/image-lazy-loading/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/video/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/input/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/textarea/?__ui__testing=true',
    'https://demo-it.wsr74.dealerk.com/elektra/checkbox/?__ui__testing=true',
  ],
  elektraComponents: [
    'badge',
    'buttons',
    'toggles',
    'image',
    'image-lazy-loading',
    'video',
    'input',
    'textarea',
    'checkbox',
  ],
};

export default baseCaps;
