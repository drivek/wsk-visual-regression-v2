const path = require('path');
const fs = require('fs');

// Reference screenshots are saved in this directory
// you absolutely should add this directory to version control.
const baselineDirectory = path.resolve(__dirname, '..', '..', 'baseline');

if (!fs.existsSync(baselineDirectory)) {
  fs.mkdirSync(baselineDirectory);
}

const outputDirectory = path.resolve(__dirname, '..', '..', 'output');

if (!fs.existsSync(outputDirectory)) {
  fs.mkdirSync(outputDirectory);
}

const isBaselineRunning = process.argv.includes('baseline');

module.exports = {
  baselineDirectory,
  outputDirectory,
  isBaselineRunning,
};
