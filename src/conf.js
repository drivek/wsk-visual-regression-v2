const config = {
  timeout: 100000,
  pause: {
    attemptLogin: 25000,
    short: 1000,
    medium: 1800,
    long: 3000,
    vLong: 10000,
  },
  reportMessage: {
    goAtHome: 'Start new epic with new scenario',
    cleanScenario: 'Clean User Story data',
  },
};
export default config;
