const { join } = require('path');

const services = {
  gridVisualRegression: {
    protocol: 'http',
    hostname: 'qa-sel-hub.docker.motork.io',
    port: 4444,
    path: '/wd/hub',
    services: [
      ['image-comparison',
      // The options
        {
        // Some options, see the docs for more
          baselineFolder: '/etc/tests/regression',
          formatImageName: `{tag}-{${process.env.VIEWPORT}}-{width}x{height}`,
          screenshotPath: join(process.cwd(), '.tmp/'),
          savePerInstance: true,
          autoSaveBaseline: true,
          blockOutStatusBar: true,
          blockOutToolBar: true,
          // NOTE: When you are testing a hybrid app please use this setting
          isHybridApp: false,
          // Options for the tabbing image
          tabbableOptions: {
            circle: {
              size: 18,
              fontSize: 18,
            // ...
            },
            line: {
              color: '#ff221a', // hex-code or for example words like `red|black|green`
              width: 3,
            },
          },
        // ... more options
        }],
    ],
  },
  local: {
    runner: 'local',
    port: 4723,
    baseUrl: 'http://localhost',
    services: [
      ['image-comparison',
      // The options
        {
        // Some options, see the docs for more
          baselineFolder: './baseLineFullPage',
          formatImageName: `{tag}-{${process.env.VIEWPORT}}-{width}x{height}`,
          screenshotPath: join(process.cwd(), '.tmp/'),
          savePerInstance: true,
          autoSaveBaseline: true,
          blockOutStatusBar: true,
          blockOutToolBar: true,
          // NOTE: When you are testing a hybrid app please use this setting
          isHybridApp: false,
          // Options for the tabbing image
          tabbableOptions: {
            circle: {
              size: 18,
              fontSize: 18,
            // ...
            },
            line: {
              color: '#ff221a', // hex-code or for example words like `red|black|green`
              width: 3,
            },
          },
        // ... more options
        }],
      ['chromedriver', {
        outputDir: 'driver-logs',
        // overwrites the config.outputDir
        args: ['--headless --silent --disable-dev-shm-usage --disable-extensions --no-sandbox'],
      }]],
  },
};
export default services;
