const browsers = {
  chromeDesktop: {
    maxInstances: 5,
    browserName: 'chrome',
    platformName: 'ANY',
  },
  chromeMobileViewport: {
    maxInstances: 5,
    browserName: 'chrome',
    platformName: 'ANY',
  },
  firefox: {
    maxInstances: 5,
    browserName: 'firefox',
    platformName: 'ANY',
  },
  chromeLocal: {
    maxInstances: 1,
    browserName: 'chrome',
  },
};

export default browsers;
