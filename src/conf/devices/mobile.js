const mobiles = {
  iPhone: {
    default: {
      browserName: 'iPhone',
      os_version: '12',
      device: 'iPhone XS',
      'browserstack.local': true,
    },
    iOS12: {
      browserName: 'iPhone',
      os_version: '12',
      device: 'iPhone XS',
    },
    iOS11: {
      browserName: 'iPhone',
      os_version: '11',
      device: 'iPhone X',
    },
    iOS10: {
      browserName: 'iPhone',
      os_version: '10',
      device: 'iPhone 7',
    },
  },
  android: {
    default: {
      os_version: '9.0',
      device: 'Google Pixel 3 XL',
      'browserstack.local': true,
    },
    9.0: {
      os_version: '9.0',
      device: 'Google Pixel 3 XL',
    },
    8.1: {
      os_version: '8.1',
      device: 'Samsung Galaxy Note 9',
    },
    8.0: {
      os_version: '8.0',
      device: 'Google Pixel',
    },
    7.0: {
      os_version: '7.0',
      device: 'Samsung Galaxy S8 Plus',
    },
  },
};

export default mobiles;
