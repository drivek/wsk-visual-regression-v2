import services from './services';
import desktop from './devices/desktop';

const specsBaseline = {
  specs: ['./dist/utility/collectors/*.js'],
};

const specsRegression = {
  specs: ['./dist/tests/**/*.js'],
};

const manageVisualCaps = (isChromeDesktop) => {
  const capabilities = [];
  if (isChromeDesktop) {
    capabilities.push(desktop.chromeDesktop);
  } else {
    capabilities.push(desktop.chromeMobileViewport);
  }
  return Object.assign(services.gridVisualRegression, { capabilities });
};

const visualCaps = (target) => {
  const capabilities = [];
  switch (target) {
    case 'Desktop viewport':
      return manageVisualCaps(true);
    case 'Mobile viewport':
      return manageVisualCaps(false);
    default:
      capabilities.push(desktop.chromeLocal);
      return Object.assign(services.local, { capabilities });
  }
};

const chooseSpecs = () => {
  let specs;
  const type = process.env.BASELINE;
  if (type) {
    specs = specsBaseline;
  } else {
    specs = specsRegression;
  }
  return specs;
};

export { visualCaps, chooseSpecs };
