Feature: Test the vdp ui

Scenario: The vdp looks fine
    Given I open the url "<url>"
    And   I have a screen that is 1440 by 7000 pixels
    Then  I do a visual check of "vdp--desktop--<site>"

Examples:
      | site      | url |
      | classic   | https://automation-it.wsk-qa.dealerk.com/auto/km0/milano/volkswagen/up/benzina/1-0-75-cv-5p-high/4622996/ |
      | revo      | https://automation-it.wsk-qa.dealerk.com/auto/usate/milano/volkswagen/up/benzina/1-0-tsi-3p-gti-bluemotion-technology/4623034/ |

