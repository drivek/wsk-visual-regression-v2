module.exports = {
    "plugins": ["jasmine"],
    "extends": "airbnb-base",
    "env": {
        "jasmine": true
    },
    "overrides": [{
        "files": "*.js",
        "excludedFiles": "wdio.conf.js",
    }],
    "globals": {
        "$": true,
        "$$": true,
        "browser": false,
        "driver": false,
    },
    "rules": {
        "class-methods-use-this": 0,
        "no-underscore-dangle": 0,
        "no-void": 0,
        "one-var": 0,
        "max-statements": [1, 25],
        "prefer-destructuring": ["error", {
            "array": false,
            "object": true
        }],
    }
};